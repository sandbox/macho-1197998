<?php
// $Id$
/**
 * @file
 * WordExtractor class for .doc word files.
 */

class WordExtractor extends DocExtractor {
  public function build() {
    $antiword_bin = trim(`which antiword`);

    $text = `$antiword_bin $this->filepath`;

    // just grab the first 150 characters of the file as title
    $this->setTitle(drupal_substr($text, 0, $this->max_title_length()));
    $this->setBody($text);
  }
}
