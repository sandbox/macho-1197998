<?php
// $Id$
/**
 * @file
 * HtmlExtractor class pulls info out of html files.
 */

class HtmlExtractor extends DocExtractor {
  public function build() {
    $file = new DOMDocument();
    $file->strictErrorChecking = FALSE;

    // @ sign suppresses errors, with which we aren't concerned.
    @$file->loadHTMLFile($this->filepath);
    $title = trim($file->getElementsByTagName('title')->item(0)->nodeValue);
    $this->setTitle($title);

    $html = file_get_contents($this->filepath);
    $html = strip_tags($html);
    $this->setBody($html);
  }
}

