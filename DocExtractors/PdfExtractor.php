<?php
// $Id$
/**
 * @file
 * PdfExtractor class
 */

class PdfExtractor extends DocExtractor {
  public function build() {
    $pdftotext_bin = trim(`which pdftotext`);

    $text = `$pdftotext_bin $this->filepath -`;

    // just grab the first 150 characters of the file as title
    $this->setTitle(drupal_substr(ltrim($text), 0, $this->max_title_length()));
    $this->setBody($text);
  }
}

