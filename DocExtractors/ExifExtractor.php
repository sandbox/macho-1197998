<?php
// $Id$
/**
 * @file
 * ExifExtractor class handles jpg, tif, wav, etc. files
 */

class ExifExtractor extends DocExtractor {
  public function build() {
    $exif = exif_read_data($this->filepath);

    // pick some misc data to use as title/content
    $fields = Array(
      "COMPUTED" => "UserComment",
//   COMPUTED.UserCommentEncoding: ASCII
      "IFD0" => "UserComment",
      "COMMENT" => "0",
      "COMMENT" => "1",
      "COMMENT" => "2");

    foreach ($fields as $key => $name) {
      $value = "";
      if (array_key_exists($key, $exif)) {

        $value = $exif[$key];
        if (array_key_exists($name, $value))
          $value = $value[$name];
        else
          $value = "";
      }

      if (drupal_strlen($value) > 0) {

        if (drupal_strlen($title) > 0)
          $title .= " | " . $value;
        else
          $title .= $value;
      }
    }

    // just grab the first 150 characters of the file as title
    $max_chars = variable_get('newsarchiver_pdf_title_chars', MAX_TITLE_LENGTH);
    $this->setTitle(drupal_substr($text, 0, $this->max_title_length()));
    $this->setBody($text);
  }
}

