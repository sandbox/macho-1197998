<?php
// $Id$
/**
 * @file
 * The DocExtractor class handles pulling data out of various download media.
 *
 * It's subclassed for specific types of files
 */

include "HtmlExtractor.php";
include "PdfExtractor.php";
include "ExifExtractor.php";
include "WordExtractor.php";

class DocExtractor {
  protected $filepath, $doc_title, $doc_body;

  // this is for when we're making random guesses at the title
  const MAX_TITLE_LENGTH = 150;

  public function __construct($in_file_path) {
    $this->filepath = $in_file_path;
    $this->doc_title = "Couldn't read file type ("
      . $this->filepath . ")";
    $this->doc_body = "";
    $this->build();
  }

  public function title() {
    return $this->doc_title;
  }

  public function body() {
    return $this->doc_body;
  }

  protected function setTitle($new_title) {
    $this->doc_title = $new_title;
  }

  protected function setBody($new_body) {
    $this->doc_body = $new_body;
  }

  public function build() {}

  static public function load($in_file_path) {

    $ini_array = parse_ini_file("extension_converters.ini", TRUE);
    foreach ($ini_array["extractors"] as $extension => $converter) {
      if (strcmp(".". $extension, drupal_strtolower(drupal_substr($in_file_path,
        0 - drupal_strlen($extension) - 1))) == 0) {
        return new $converter($in_file_path);
      }
    }

    // default for unknown file extensions, just append ".html"
    return new DocExtractor($in_file_path);
  }

  static public function improper_extension($in_file_path) {

    $file_ext = pathinfo($in_file_path);
    $file_ext = drupal_strtolower($file_ext['extension']);
    $ini_array = parse_ini_file("extension_converters.ini", TRUE);
    $extractors = $ini_array['extractors'];

    foreach ($extractors as $extension => $converter) {
      if (strcmp($extension, $file_ext) == 0)
        return FALSE;
    }

    return TRUE;
  } // function improper_extension()

  protected function max_title_length() {
    return variable_get('newsarchiver_pdf_title_chars', MAX_TITLE_LENGTH);
  }
}

