// ==UserScript==
// @name        Archiver Shortcut Key
// @description Ctrl+Alt+A archives any page using Drupal's newsarchiver module
// @namespace   http://macho.bike-dump.ca/
// @include     *
// @revision    $Revision$
// @id          $Id$
// @date        $Date$
// @source      $URL$
// @author      Macho Philipovich
// @version     0.2
// ==/UserScript==

var YOUR_DRUPAL_DOMAIN = "http://macho.bike-dump.ca";
var ARCHIVER_PATH = "/newsarchiver";
var already_visited = false;
var DEBUG = false;

document.addEventListener('keyup', keyPressHandler, false);

function keyPressHandler(event) {
    
  /* Ctrl+Alt+A */
  if (event.ctrlKey && event.altKey && event.charCode==97) {

    if (already_visited)
      return;
    else
      already_visited = true;

    if (YOUR_DRUPAL_DOMAIN == "http://url.not.set") {
      alert("You need to set 'YOUR_DRUPAL_DOMAIN' for this to work properly.");
      return;
    }

    GM_xmlhttpRequest( {
      method: "POST",
      url: YOUR_DRUPAL_DOMAIN + ARCHIVER_PATH,
      data: "news_url=" + encodeURIComponent(document.URL) + "&"
        + "op=Submit&form_id=submit_url_form",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      onload: function(response) {
        /* This tab, normally should use location.replace,
          but in this case, I don't want a new browser history entry */
        // location.href = response.finalUrl;
        /* New tab */
        GM_openInTab( response.finalUrl );
        
        if(DEBUG) {
          alert(
            "status: "+response.status+"\n"
            +"statusText: "+response.statusText+"\n"
            +"readyState: "+response.readyState+"\n"
//          +"responseHeaders: "+response.responseHeaders+"\n"
            +"finalUrl: "+response.finalUrl+"\n"
          );
        }
      }
    });
  }
}

