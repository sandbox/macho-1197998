<?php
// $Id$
/**
 * @file
 * Handles the url submission form, which downloads the link and processes its
 * contents, passing the info on to a form to add a newsarchive node.
 */

require_once('DocExtractors/DocExtractor.php');

/**
 * Definition of the form to submit a url
 */
function submit_url_form(&$form_state) {

  // URL field
  $form['news_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the url'),
    '#maxlength' => 512,
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
} // function submit_url_form()

/**
 * Form validation callback. No submit callback is necessary, since if the
 * form validates, this function redirects the user, passing in the data.
 */
function submit_url_form_validate($form, &$form_state) {

  $orig_url = check_url($form_state['values']['news_url']);
  $url = $orig_url;

  $file_fullpath = make_output_filename($url);

  $cerr1 = download_url($url, $file_fullpath);
  if (drupal_strlen($cerr1) > 0) {
    // Try removing the fragment
    $url = explode("#", $url);
    $url = $url[0];
    unlink($file_fullpath);

    $cerr = download_url($url, $file_fullpath);
    if (drupal_strlen($cerr) > 0) {
      // Try also reming the query
      $url = explode("?", $url);
      $url = $url[0];
      unlink($file_fullpath);

      $cerr = download_url($url, $file_fullpath);
      if (drupal_strlen($cerr) > 0) {
        // No luck
        form_set_error('news_url', $cerr1);
        unlink($file_fullpath);
        return;
  } } }

  parse_file_and_redirect($file_fullpath, $url);

} // function submit_url_form_validate()

/**
 * Make a reasonable filename from a url, ensure its directory exists, and
 * that the file doesn't already exist.
 */
function make_output_filename($url) {

  // Ensure the directory exists.
  $dest_dir = file_directory_path() . '/'
    . variable_get('newsarchiver_default_path', 'newsarchive') . '/'
    . date('Y/m/d');
  if (!file_exists($dest_dir))
    mkdir($dest_dir, 0777, TRUE);

  // Grab whatever human-readable part of the url we can decipher.
  $dest_file = parse_url($url);
  $dest_file = $dest_file['path'];
  $dest_file = explode("/", $dest_file);
  $dest_file = $dest_file[count($dest_file)-1];
  while (drupal_strlen($dest_file) > 0 && $dest_file[0]=='.') {
    $dest_file = drupal_substr($dest_file, 1);
  }
  if (drupal_strlen($dest_file) == 0)
    $dest_file = "index";
  // Force it to use the .html suffix, so the browser will display it.
  if (DocExtractor::improper_extension($dest_file))
    $dest_file .= ".html";

  $info = pathinfo($dest_file);

  // If the filename already exists, adjust it until it does not
  $append = 1;
  while (file_exists($dest_dir . '/' . $dest_file)) {
    $dest_file = $info['filename'] . '-' . strval($append++) .
      '.' . $info['extension'];
  }

  return $dest_dir .'/'. $dest_file;
} // function get_output_filename()

function download_url($url, $filename) {
  $cerr = curl_download($url, $filename);

  // if the error is ssl related try forcing sslv3
  if (stripos($cerr, "ssl") != FALSE)
    $cerr = curl_download($url, $filename, 3);

  return $cerr;
}

function curl_download($url, $filename, $sslversion=NULL) {

  $ch = curl_init($url);
  $file = fopen($filename, "w");
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
  curl_setopt($ch, CURLOPT_FILE, $file);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  if ($sslversion != NULL)
    curl_setopt($ch, CURLOPT_SSLVERSION, $sslversion);

  curl_exec($ch);
  $cerr = curl_error($ch);
  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);
  if ($httpcode < 200 || $httpcode >= 300) {
    $cerr .= ', HTTP '. $httpcode;
  }

  return $cerr;
} // function download_url()

function parse_file_and_redirect($filename, $url) {

  $docdata = DocExtractor::load($filename);

  /* drupal_urlencode was messing stuff up, so i didn't use it below */
  $destination = 'edit[title]='. urlencode($docdata->title())
    .'&'.  get_tag_suggestions($docdata->body()) .
    'edit[url]='. urlencode($url) .'&'.
    'edit[location]='. drupal_urlencode($filename);

  // It's validated, so send them to a form input page...
  drupal_goto('node/add/newsarchiver', $destination);
} // function parse_file_and_redirect()

/**
 * @return A list of tags from our vocabulary also found in the file.
 */
function get_tag_suggestions($text) {

  $vid = variable_get('newsarchiver_vocabulary_vid', NULL);
  $vocab = taxonomy_get_tree($vid);
  $list = 'edit[taxonomy][tags]['. $vid .']=';

  foreach ($vocab as $term) {
    // is this term in the file
    if (stripos($text, $term->name) != FALSE)
      $list .= drupal_urlencode($term->name) . ', ';
  }

  $list = trim($list, ", ");

  $list .= '&';

  return $list;
} // function get_tag_suggestions()

/**
 * given a url/filename, do we need to add '.html' to make it readable
 */
function improper_extension($dest_file) {

  // html or htm are fine
  if (strcmp(".html", drupal_substr($dest_file, -5)) == 0
    || strcmp(".htm", drupal_substr($dest_file, -4)) == 0)
    return FALSE;

  $extensions = variable_get('newsarchiver_non_html_extensions',
    array('pdf', 'doc', 'jpg', 'png', 'gif'));
  foreach ($extensions as $ext) {
    if (strcmp(".". $ext, drupal_substr(
      $dest_file, 0 - drupal_strlen($ext) - 1)) == 0)
      return FALSE;
  }

  return TRUE;
} // improper_extension
