 
-- SUMMARY --

The NewsArchiver module is for people who

* find that web news items they'd like to refer to have disappeared from the 
  sites where they read them;
* are sometimes unable to find news items they once remember reading; or
* would like to be able to tag news items for future reference.

For a full description of the module, visit the project page:
  http://drupal.org/project/newsarchiver

To submit big reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/newsarchiver

-- REQUIREMENTS --

* date
* date_popup
* prepopulate
* taxonomy
* simplehtmldom

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* If you'd like, you can also install the Greasemonkey script found here in
  ArchiverShortCutKey-x.x.user.js
  
* You can find the Greasemonkey Firefox add-on here:
  https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/

* And instructions on using Greasemonkey scripts here:
  http://greasemonkey.mozdev.org/using.html

-- CONFIGURATION --

None.

-- CUSTOMIZATION --

None.

-- TROUBLESHOOTING --

None.

-- FAQ --

None.

-- CONTACT --

Current maintainer:
* Macho Philipovich http://drupal.org/user/435534

